import { Note } from "./note";
import NoteStore from "./noteStore";

let first = new Note();
first.title = "test";
first.text = "to jest test";

let second = new Note("test2", "to jest test2");

let store = new NoteStore();
store.save(first);
store.save(second);

let savedData = store.load();
savedData.forEach((x) => {
        console.log(`Title: ${x.title}`);
        console.log(`Text: ${x.text}`);
        console.log(`date: ${x.date}`);
        console.log(`\n`);
});

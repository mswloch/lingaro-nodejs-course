import { Note } from "./note";
import fs from "fs";

export default class NoteStore {
  private storePath: string = "noteStorage.json";

  public constructor() {}

  public save(note: Note) {
    if (!fs.existsSync(this.storePath)) {
      let noteArray = [note];
      fs.writeFileSync(this.storePath, JSON.stringify(noteArray));
      return;
    }

    let data = fs.readFileSync(this.storePath, "utf8");
    let noteArray = JSON.parse(data);
    noteArray.push(note);

    fs.writeFileSync(this.storePath, JSON.stringify(noteArray));
  }

  public load(): Array<Note> {
    let data = fs.readFileSync(this.storePath, "utf8");
    let noteArray = JSON.parse(data);

    return noteArray;
  }
}

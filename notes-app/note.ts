export class Note {
  public title: string;
  public text: string;
  public date: Date;

  public constructor(text?: string, title?: string, date?: Date) {
    this.title = title != null ? title : "";
    this.text = text != null ? text : "";
    this.date = date != null ? date : new Date();
  }
}

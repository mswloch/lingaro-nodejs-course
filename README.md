# TODO LIST
* jak formatować kod? dobre praktyki.
* vscode i dodatki
* npm
* hello world
* api
* klasy i podział aplikacji
* threads i async
* kody błędu
* dokumentacja, autogenerowanie
* orm i połączenie do bazy danych
* KeyVault i inne zabezpieczenia danch wrażliwych
* testy jednostkowe
* architektura
* modułowość
* boilerplaiting
* CORS
* AD
* CQRS
* EventSourcing

# jak formatować kod? dobre praktyki

https://basarat.gitbook.io/typescript/styleguide

* metody z małej litery

^mistrzu^ - '' vs ""
# vscode i dodatki

1. Praca na dwóch monitorach 

ctrl+shift+p (prompt z opcjami) -> Workspaces: duplicate as workspaces in new window (tworzy nową instancje vs). 
Przy przenoszeniu okna, warto upewnić się czy plik jest zapisany, bo vs nie przenosi okna, tylko tworzy nowa instancje tego okna w drugim edytorze.

## lista dodatków

^mistrzu^
intellisence dla js?
autoimport (require?)
ma mi np podpowiadać jakie metody zawiera fs

* Node.js Modules Intellisense
*
*

# npm
## instalacja typescriptu i generowanie tsconfig
npm install typescript -g     
tsc --init  

wyszukiwarka bibliotek: npmjs.com

Globalne instalowanie npm nie jest najlepszym pomysłem przy pracy grupowej. Change my mind.

NODEMON - hotloader, jak skonfigurować z ts?

## komendy
 
 - node [nazwapliku] //wykonanie pliku, (^mistrzu^ kompilacja, linkowanie itp ???) 

# hello world

^mistrzu^ Jak poprawnie formatować console.log, warn, error, info w terminalu? ~ hello-world/consolelogs.js, nie widze różnic w terminalu

Ułomność JSON.parse -> nie tworzy poprawnych instancji class, trzeba je tworzyć ręcznie i przypisywać dane lub odizolować logikę od modelu, MVC w takim razie ssie

Chrome DevTools w kontekście node.js są useless, change my mind.